﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeeetoditeOverloadimine
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Liida(1, 2));

            Console.WriteLine(Korruta(b: 5.0, a: 10));
        }

        static double Liida(double a, int b)
        {
            return a + b;
        }


        static double Liida(double a, double b)
        {
            return a + b;
        }

        static double Korruta (double a = 6, double b = 7)
        {
            return a * b;
        }


    }
}
